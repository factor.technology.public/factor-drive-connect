import http.server
import http.client
import urllib
import signal
import sys
from urllib.parse import urlparse, parse_qs

class Proxy(http.server.SimpleHTTPRequestHandler):
    remote_host = 'drive.factor.technology'  # Replace with your remote HTTPS server

    def do_GET(self):
        self.proxy_request("GET")

    def do_POST(self):
        self.proxy_request("POST")

    def do_PUT(self):
        self.proxy_request("PUT")

    def do_HEAD(self):
        self.proxy_request("HEAD")

    def do_DELETE(self):
        self.proxy_request("DELETE")

    def proxy_request(self, method):
        conn = http.client.HTTPSConnection(self.remote_host)
        content_length = int(self.headers['Content-Length']) if 'Content-Length' in self.headers else 0
        body = self.rfile.read(content_length) if content_length > 0 else None
        headers = {k: v for k, v in self.headers.items() if k != 'Host'}
        conn.request(method, self.path, body, headers)
        resp = conn.getresponse()

        self.send_response(resp.status)
        self.send_header('Content-type', resp.headers['Content-Type'])
        self.end_headers()

        self.wfile.write(resp.read())

def run(server_class=http.server.HTTPServer, handler_class=Proxy, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting httpd on port {port}...')
    httpd.serve_forever()

def signal_handler(sig, frame):
    print('bye')
    sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    run(port = 8765)