
# Factor Drive Python Tools

[Factor Drive](https://www.factor.technology) users can manage computed geosteering 
jobs programatically. This package contains `fdrive`, a command line tool for
managing jobs and data, and `drive_api` exposing the same functionality to Python
programs.

## Installation
```
pip install fdrive
```

You must create a configuration file named `.driverc.yaml` and place it in your home directory. The file must contain your API token, which you obtain by logging
in to Factor Drive and visting your [API token](https://drive.factor.technology/jwt)
page. Copy your token to `.driverc.yaml` which typically will have a single line:

```yaml
token: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

Since your `.yarnrc.yaml` file contains your secret API token, you should set permissions on it so that only you can read it, amd prevent backup tools from 
copying it.

## fdrive commands
Get help on each of these commands by running `fdrive COMMAND --help`. 
```
    create_pilot_well
       Create pilot well.

     create_project
       Creates a new project.

     delete_project
       Deletes a project.

     get_job_status
       Gets the job status for a project.

     get_lwd
       Gets LWD data for a project.

     get_mpe
       Gets the MPE (the computed horizon) for a project.

     get_pilot_log
       Gets the pilot log for a project.

     get_project
       Gets a project.

     list_projects
       Lists all projects permitted to the current user.

     put_lwd_samples
       Puts LWD data for a project.

     put_pilot_log
       Update the pilot log for a project.

     put_pilot_well
       Update the pilot well for the project.

     put_project
       Updates an existing project.

     put_trajectory
       Puts trajectory data for a project.

     reset_job
       Resets a job for a project.

     run_job
       Runs a job for a project.
```

## JSON data
The commands may accept and return JSON data. If a command argument accepts JSON,
it expects a quoted string containing the data. Alternatively, if you precede
the argument with `@`, `fdrive` will interpret the argument as a file name and load
the JSON from that file.


## Examples
```sh
fdrive create_project hw1/malobar 

fdrive put_lwd_samples hw1/malobar --mnemonic GR --samples @lwd-samples.json 

fdrive get_lwd hw1/malobar

fdrive put_trajectory hw1/malobar @active-trajectory.json

fdrive create_pilot_well hw1/malobar 'Pilot Well 1'

fdrive put_pilot_log hw1/malobar @pilot-log-samples.json GR --trajectory @pilot.trajectory.json 

fdrive reset_job hw1/malobar

fdrive run_job hw1/malobar

fdrive get_job_status hw1/malobar

fdrive get_mpe hw1/malobar 
```

### JSON files
lwd-samples.json:
```json
[
    [
      1601,
      99.6483
    ],
    [
      1602,
      99.1273
    ],
    ...
]
```

active-trajectory.json:
```json
{
  "waypoints": [
    {
      "md": 0,
      "azi": 0,
      "incl": 0
    },
    {
      "md": 14.0,
      "azi": 40.87,
      "incl": 0
    },
    {
      "md": 96.0,
      "azi": 40.87,
      "incl": 0.75
    },
	...
  ]
}
```

### Workflow
You can modify project parameters like so:

```sh
fdrive get_project hw1/malobar > malobar.json
```
Output file `malobar.json`:
```json
{
  "z0": -5000,
  "name": "malobar",
  "scope": "hw1",
  "md_last": 17973,
  "dip_type": "constants",
  "interval": 12,
  ...
  "md_first_to_compute": 8083.401180382987,
  "active_datum_elevation": 4755
}
```
Edit `malobar.json`, modifying `md_first_to_compute: 7500`.
Then update the project in Factor Drive, reset, amd rerun the job:

```
fdrive put_project @malobar.json

fdrive reset_job hw1/malobar

fdrive run_job hw1/malobar
```