
# Copyright © 2024 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
./fdrive.py create_project hw1/malobar 
./fdrive.py put_lwd_samples hw1/malobar --mnemonic GR --samples @~/tmp/hw-DJ-G-2N/lwd-samples.json 
./fdrive.py get_lwd hw1/malobar
./fdrive.py put_trajectory hw1/malobar @~/tmp/hw-DJ-G-2N/active-trajectory.json
./fdrive.py create_pilot_well hw1/malobar 'Pilot Well 1'
./fdrive.py put_pilot_log hw1/malobar @../Factor\ Connect/example/pilot-log-samples.json GR --trajectory @../Factor\ Connect/example/hw-DJ-G-2N--pilot.trajectory.json 
./fdrive.py reset_job hw1/malobar
./fdrive.py run_job hw1/malobar
./fdrive.py get_job_status hw1/malobar
./fdrive.py get_mpe hw1/malobar 
