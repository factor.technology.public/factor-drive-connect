# Copyright © 2021 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Common functions used to manipulate SS data.
"""
import math

def load_log(well, gamma):
    """
    Load log data.
    """
    log_data = []

    for depth in well.Logs[gamma]:
        if not math.isnan(depth.value):
            log_data.append([depth.MD, depth.value])

    return (log_data, gamma)

def load_waypoints(well):
    """
    Load the example trajectory from the first survey up to `md_limit`
    Return a list of [md, inc, azi] triples.
    """
    surveys = []

    for surv in well:
        survey = {
            'md': surv.MD,
            'azi': round(surv.Az, 3),
            'incl': round(surv.Incl, 3),
            'tvd': round(surv.TVD, 3),
            'east': round(surv.X - well.Xsrf, 5),
            'north': round(surv.Y - well.Ysrf, 5),
            'vs': round(surv.VS, 3)
        }

        surveys.append(survey)

    if len(surveys) < 2:
        surveys = None

    return surveys
