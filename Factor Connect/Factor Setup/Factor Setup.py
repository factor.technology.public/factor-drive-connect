# pylint: disable=import-error,undefined-variable,bare-except
# pylint: disable=invalid-name,missing-module-docstring,line-too-long
from importlib import reload
import drive_config
import drive_api
import factor_setup
from factor_setup import run_setup

reload(drive_config)
reload(drive_api)
reload(factor_setup)

config = drive_config.DriveConfig(
    token = token,
    host = host,
    scope = scope,
    is_offset_fixed = is_offset_fixed,
    autofit_window_height = autofit_window_height
)

well = Wells[ActiveLateralName]
typewell = Wells[typewellName]

welGam = well.Logs[gammaLog]
typGam = typewell.Logs[typewellGamma]

typewellShift = well.getTypewellShift(typewellName)

KB = well.KB
vsAz = well.AzimuthVS
typKB = typewell.KB

faultData = [faultCount, faultMinSize, faultMaxSize]
if faultCount > 3:
    depthRes = 1
elif faultCount > 1:
    depthRes = 0.5
else:
    depthRes = 0.25

if JobParams_Type == "Standard":
    JobParams_LogSigma = 15
    JobParams_LogUncertainty = "Normal"
    JobParams_DipSigma = 2
    JobParams_DepthResolution = depthRes
    JobParams_Pruning = .00001
elif JobParams_Type == "Flexible":
    JobParams_LogSigma = 10
    JobParams_LogUncertainty = "Laplace"
    JobParams_DipSigma = 3
    JobParams_DepthResolution = depthRes
    JobParams_Pruning = .00001
elif JobParams_Type == "Strict":
    JobParams_LogSigma = 10
    JobParams_LogUncertainty = "Normal"
    JobParams_DipSigma = 2
    JobParams_DepthResolution = depthRes
    JobParams_Pruning = .00001

JobParams = JobParams_LogSigma, JobParams_LogUncertainty, JobParams_DipSigma, JobParams_DepthResolution, JobParams_Pruning

interp = well.Interpretations[ActiveInterpretationName]

setup_data = well, welGam, typewell, typGam, KB, vsAz, typKB, typewellShift, wellDip, faultData, JobParams, interp

run_setup(config, setup_data)
