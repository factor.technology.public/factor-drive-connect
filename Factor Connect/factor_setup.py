# Copyright © 2021 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
This is an example program that you can use as a starter template to script Factor Drive from
StarSteer. There's a companion module named `drive_api.py` you should not need to modify.
"""
# pylint: disable=line-too-long
# pylint: disable=missing-function-docstring

# pylint: disable=global-statement,invalid-name
# pylint: disable=bare-except,global-statement

from drive_config import DriveConfig

from shared_utils import load_log
from shared_utils import load_waypoints

from drive_api import initialize_drive_api
from drive_api import apparent_dip_to_true_dip_and_strike
from drive_api import delete_project
from drive_api import create_project
from drive_api import create_pilot_well
from drive_api import put_pilot_log
from drive_api import put_lwd_samples
from drive_api import put_trajectory
from drive_api import log_normalization_fit_params
from drive_api import update_pilot_well


# start_md = None

def run_setup(config: DriveConfig, setup_data):
    """
    Create or replace a project on the Drive server.
    """

    # The beginning of the main program.
    # You must initialize the drive_api module with the host name and your API token.
    initialize_drive_api(
        drive_host=config.host,
        port=config.port,
        token=config.token,
        secure=config.secure)

    well = setup_data[0]
    welGam = setup_data[1]
    typewell = setup_data[2]
    typGam = setup_data[3]
    KB = setup_data[4]
    vsAz = setup_data[5]
    typKB = setup_data[6]
    typewellShift = setup_data[7]
    wellDip = setup_data[8]
    faultData = setup_data[9]
    JobParams = setup_data[10]
    interp = setup_data[11]

    faultCount = faultData[0]
    faultMinSize = faultData[1]
    faultMaxSize = faultData[2]
    faultBias = 0.5

    JobParams_LogSigma = JobParams[0]
    JobParams_LogUncertainty = JobParams[1].lower()
    JobParams_DipSigma = JobParams[2]
    JobParams_DepthResolution = JobParams[3]
    JobParams_Pruning = JobParams[4]

    # Use the apparent dip to compute the (dip, strike) we need to mass to Drive.
    dip, strike = apparent_dip_to_true_dip_and_strike(
        wellDip, vsAz)

    shift, start_md = getShift(well, interp, typewell)

    print(f"typewell Shift = {typewellShift}")
    print(f'shift = {shift}')

    project = {
        'name': well.name,
        'dip_constant': dip,
        'strike_constant': strike,
        'vs_azimuth': vsAz,
        'active_datum_elevation': KB,

        'md_marker': start_md,

        'fault_num_total': faultCount,
        'fault_throw_min': faultMinSize,
        'fault_throw_max': faultMaxSize,
        'fault_up_weight': faultBias,

        'dip_sigma': JobParams_DipSigma,
        'delta_spatial': JobParams_DepthResolution,
        'log_distribution': JobParams_LogUncertainty,
        'log_sigma': JobParams_LogSigma,

        # No need to modify these for the near future:
        'dip_type': 'constants',
        'md_first_to_compute': start_md,
        'md_last_to_compute': 99999,
        'interval': 12,
        'discard_all_but_last_log_segment': False,
        'cull_factor': JobParams_Pruning,

        # Historical artifacts. Ignore:
        'sigmaDipCurv': 0.01,
        'inclination_sigma': 0.01,
        'sigma_md': 0.04
    }

    scope = config.scope
    # delete the project if it exists.
    delete_project(scope, well.name)

    # create the project.
    create_project(scope, project)

    create_pilot_well(scope,
                      well.name,
                      typewell.name,
                      typKB,
                      0)

    # add data/trajectory for the type well.
    samples, mnemonic = load_log(typewell, typGam.name)

    ### note that 'pilot_waypoints' [and 'trajectory'] are not used here!
    pilot_waypoints = load_waypoints(typewell)
    print(f"pilot_waypoints = {pilot_waypoints}")

    trajectory = {
        'waypoints': pilot_waypoints
    } if not pilot_waypoints is None else None

    print(f"Trajectory = {trajectory}")
    ###

    put_pilot_log(
        scope,
        project_name=well.name,
        mnemonic=mnemonic,
        samples=samples,
        trajectory=None,
        datum_elevation=typKB)

    # Upload Surveys/Gamma
    samples, mnemonic = load_log(well, welGam.name)
    put_lwd_samples(scope, well.name, mnemonic=mnemonic, samples=samples)

    well_waypoints = load_waypoints(well)
    put_trajectory(scope, well.name, well_waypoints)

    # Perform typewell shift and log normalization
    fit_params = log_normalization_fit_params(
        scope, well.name, shift, config.is_offset_fixed, config.autofit_window_height)

    update_pilot_well(scope, well.name, fit_params)
    print("Setup complete.")


def getShift(well, interp, typewell):
    firstMD = well[1].MD
    prevMD = prevTVT = zeroTVTmd = 0
    counter = 0
    for md in range(int(firstMD), int(well[-1].MD)):
        # counter to prevent full wellbore run. Will likely never
        # reach the full 2,000ft to cancel

        # find an ideal starting point.
        # > 60° Inc omits needless vertical/curve section
        try:
            if well.valueByMD[md].Incl > 60:
                TVT = interp.getTVTByMD(md)
                # Get closest MD to 0TVT. Once TVT goes from
                # negative to positive, the two depths are compared
                # and the closest to 0 is the MD chosen for Z.
                # The two values are typically less than 0.1' off 0 TVT
                if TVT == 0:
                    zeroTVTmd = md
                if TVT > 0:
                    if abs(TVT) < abs(prevTVT):
                        zeroTVTmd = md
                    else:
                        zeroTVTmd = prevMD
                    print(f"result: md = {zeroTVTmd}")
                    break
                prevMD = md
                counter += 1
            if counter == 500:
                zeroTVTmd = md
                break
        except:
            print(f"Warning: Unable to calculate at {md}' MD")
    # Gets the entry point of payzone to begin the Factor steer
    start_md = zeroTVTmd

    # Get the TVDSS (Z) of 0 TVT.
    # This is an attempt to not require the user to enter Pay Top
    # manually.

    zeroTVTZ = well.valueByMD[zeroTVTmd].Z

    adjusted_zeroTVTZ = zeroTVTZ + interp.getTVTByMD(start_md)
    print(f'adjusted: {adjusted_zeroTVTZ}')

    shift = getHorizonShift(interp, typewell, zeroTVTZ)
    return (shift, start_md)


def getHorizonShift(interp, typewell, zeroTVTZ):
    zeroTVTHorz = ""
    try:
        zeroTVTHorz = ""
        for horizon in interp.Horizons:
            if horizon.TVT == 0:
                zeroTVTHorz = horizon.name
        firstSet = ""
        for topset in typewell.TopSets:
            firstSet = topset.name
        for top in typewell.TopSets[firstSet]:
            if top.name == zeroTVTHorz:
                totalDiff = zeroTVTZ - top.Z
                return round(totalDiff, 1)
    except:
        print(
            f"Typewell is missing {zeroTVTZ} as a top in at least one of its topsets. \nFactor's result will not properly align in StarSteer!")
        return 0
