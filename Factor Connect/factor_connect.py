# Copyright © 2021 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
This is an example program that you can use as a starter template to script Factor Drive from
StarSteer. There's a companion module named `drive_api.py` you should not need to modify.

This program simulates a growing interpretation. Every 5 minutes it adds another 1000 feet
of LWD and trajecotry. In real life you'd get that data from StarSteer. In this simulation
we load it from JSON files.

This program doesn't set up a Factor Drive project from scratch. It assumes you have already
created the project 'hw-DJ-G-2N', that you've collected data at least down to the top of
target, and that you've set the whole project up (loading LAS and trajectory, aligned
and scaled the type log to fit the LWD). That's a manual process you do in the Factor Drive
web app.

Once that's done, you're ready to grow the lateral with a program like this.

This program starts a timer thread that runs every 20 seconds. Each time it runs, it retrieves
the current computed structure, and the status of any processing that's running, from Factor Drive.
Whenever another five minutes elapses, it loads 1000 more feet of new LAS and trajectory data,
as if someone had loaded more data into SS. Then, when we see the longer data, we submit
it to Factor Drive and run a job to update the computation.
"""
# pylint: disable=bare-except,global-statement,invalid-name,line-too-long,missing-function-docstring

import math

from drive_config import DriveConfig

from drive_api import DriveAPIException
from drive_api import initialize_drive_api
from drive_api import get_mpe
from drive_api import put_lwd_samples
from drive_api import put_trajectory
from drive_api import run_job

from shared_utils import load_log
from shared_utils import load_waypoints

def sync_with_drive(config: DriveConfig, project_name, well, gammaLog, unsmoothedinterp):
    """
    Process:
        -Get factor results in MD, Z pairs
        -Delete all data in unsmoothedinterp up to first factor datapoint
        -Get block shift
        -Convert MD, Z to MD, Dip, Throw
        -Enter converted data into unsmoothedinterp
        -Combine identical blocks to minimize redundancy
    """
    try:
        #Make connection with Factor
        initialize_drive_api(
            config.host, config.token, config.secure, config.port)

        scope = config.scope

        #convert mpe_original to mpe to attempt to align data (Not working right now)
        mpe = get_mpe(scope, project_name)

        #clear original interpretation data back to the first Factor depth.
        #This retains all of the landing blocks
        wipeInterp(unsmoothedinterp, mpe[0][0])

        #Get blockshift value. This is done by getting the first depth of the
        #interpretation and its TVDSS value (or, Z). It is then extrapolated
        #out to the payzone top. The closest MD to 0TVT is acquired, then its
        #Z value is acquired. The blockshift is the difference between these
        #two shift values.

        #The shift occurs because Factor interpolates a 90° block from start of
        #the well to landing point. However, many wells are steered with faults
        #or dips, which causes a shift of a few feet.
        # shift = 0#getBlockShift(well, unsmoothedinterp, mpe)
        # n.b. I removed unused getBlockShift code 1 Dec 2021 hvw

        #Convert the mpe data of [MD, Z] to [MD, Dip, Throw].
        #It works by performing a tangent calculation of the current depth's Z
        #to the next depth's Z. If the dip is very significant (set to <70 or >110),
        #It will be set to a 90° dip and a throw will be designated instead of the dip.
        convertedData = []
        convertZtoDip(convertedData, mpe)

        #Since landing data is retained, we need to get the "0"th segment, which
        #will be the last segment of the retained landing blocks, and this becomes
        #the initial point for these calcs
        u_firstSegNum = len(unsmoothedinterp)

        #Enters the convertedData array to the interpretation. The first block is
        #shifted by the shift variable. This function uses StarSteer's interpretation
        #object functions of adding segments and setting depth, dip, and throw
        importFactorSteer(convertedData, unsmoothedinterp, u_firstSegNum) #, shift, blockShift)

        #Factor data is in the form of 12' blocks, with some exceptions, but
        #as a result there are many redundant blocks. The large quantity of
        #12' blocks slows down StarSteer and makes it difficult to select blocks.
        combineRedundantBlocks(unsmoothedinterp, u_firstSegNum)

        print("Interpretation successfully imported to StarSteer.")
    except DriveAPIException as e:
        print("No data retreived. If this is you first run of factorconnect for this well, this is expected."
              "\nPlease wait a few minutes for Factor to create an interpretation based on the data you just sent.\n")
        #print(f'get_mpe failed: {e}')

    # As a job is processing you can get progress info from the status. Strictly for information purposes. Format as desired
    # try:
    #     status = get_job_status(scope, project_name)
    #     print(status)
    # except DriveAPIException as e:
    #     pass
    #     #print(f'get_job_status failed: {e}, but don''t sweat it.')

    # Get the latest LWD and trajectory from Star Steer.
    samples, mnemonic = load_log(well, gammaLog)
    surveys = load_waypoints(well)

    # Check whether the `samples` and `md_incl_azi_triples` are longer than the previous ones.
    # If they are longer, submit them to Factor Drive and run a job.
    # if len(samples) > prev_samples_len and len(surveys) > prev_trajectory_len:
    # prev_gamLog_len = len(gammaLog)
    # prev_surv_len = len(surveys)
    try:
        #if newDataPresent:
        # Send the LWD
        print('send LWD')
        put_lwd_samples(scope, project_name, mnemonic=mnemonic, samples=samples)

        # Send the trajectory
        print('send trajectory')
        put_trajectory(scope, project_name, surveys)

        # Kick off processing of that new data
        # run_job returns as soon as the job starts. The job runs remotely, and we check
        # its status every time through the loop using `get_job_status`
        print('run job')
        run_job(scope, project_name)

        print("Surveys and Gamma successfully submitted to Factor. "
              "\nPlease allow a few minutes for Factor to interpret the data.")

    except DriveAPIException as e:
        print(f'Failed updating Factor Drive with new data: {e}')

def wipeInterp(unsmoothedinterp, firstFactorDepth):
    maxSeg = len(unsmoothedinterp.Segments) - 1

    for seg in range(maxSeg):
        if unsmoothedinterp.Segments[maxSeg - seg].MD >= firstFactorDepth:
            unsmoothedinterp.deleteSegment(maxSeg - seg)

def convertZtoDip(convertedData, mpe_local):
    # throw = nextThrow = 0
    for i in range(len(mpe_local) - 1):
        depth = mpe_local[i][0]
        # convert Z to Dip.
        try:
            dip = 90 + math.degrees(math.atan((mpe_local[i + 1][1] - mpe_local[i][1]) / (mpe_local[i + 1][0] - mpe_local[i][0])))
            #print(f'At {depth}, dip = {dip}')
            if dip < 70 or dip > 110:
                dip = 90
                nextThrow = mpe_local[i][1] - mpe_local[i + 1][1]
            else:
                nextThrow = 0
            #print(f"throw = {throw}, nextThrow = {nextThrow}")
        except:
            print("error here")
            nextThrow = 0
            dip = 90
        # Add to block array
        convertedData.append([depth, dip, nextThrow])
        #print(f"result: {convertedData[i]}")
        # throw = nextThrow

def importFactorSteer(convertedData, unsmoothedinterp, u_firstSegNum):
    # Apply Factor's results, inputting to the Factor interpretation
    for i in range(0, len(convertedData) + 1):
        element = i
        try:
            unsmoothedinterp.addSegment(convertedData[element][0])
            #print(f"Test: {convertedData[element]}")
            seg = unsmoothedinterp.Segments[i + u_firstSegNum]
            seg.dip = convertedData[element][1]

            #if abs(convertedData[element][2]) > 0:
            seg.throw = convertedData[element][2]
        except:
            print(f"Warning: error at block {element} while importing.")

def combineRedundantBlocks(unsmoothedinterp, u_firstSegNum):
    # combine blocks with same dip to reduce block count
    try:
        for i in range(u_firstSegNum, len(unsmoothedinterp) - 1):
            nextSame = True
            seg = unsmoothedinterp.Segments[i]
            #print(f"Running through {seg.MD}' MD. dip = {seg.dip}, throw = {seg.throw}")
            while nextSame:
                nextItem = 1
                nextSeg = unsmoothedinterp.Segments[i + nextItem]
                nextItem += 1
                if round(seg.dip,1) == round(nextSeg.dip, 1) and not abs(nextSeg.throw) > 0:
                    #print(f"Merging to {seg.MD}")
                    unsmoothedinterp.mergeNext(i)
                else:
                    nextSame = False
    except:
        pass
