# Copyright © 2021 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
This is an example program that you can use as a starter template to script Factor Drive from
StarSteer. There's a companion module named `drive_api.py` you should not need to modify.
"""
# pylint: disable=global-statement,invalid-name
import json

from drive_api import initialize_drive_api
from drive_api import get_pilot_well
from drive_api import update_pilot_well
from drive_api import log_normalization_fit_params

# Set constants we'll use.
# Use your own API token here:

# pylint: disable=line-too-long
TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Mjk5OTA5ODMsImF1ZCI6Ii9hcGkvZHJpdmUvdjEiLCJpc3MiOiJGYWN0b3IgVGVjaG5vbG9neSIsInN1YiI6Imh1Z2gud2lua2xlciIsImp0aSI6Ijg4ZjljNmNlLTQxYWUtNDJlOS05YzRhLTczYmI2MzE5ZDdmNyJ9.Tx3asUZdh4KTn6WovDRu2XSbjONxi3i59ZD-_baRFew'
# pylint: enable=line-too-long
PROJECT_NAME = 'hw DJ G 2N'
DRIVE_HOST = 'localhost'
PORT=3000
SECURE=False


# TOKEN=''
# DRIVE_HOST = 'localhost'
# PORT=3000
# SECURE=False

project_name = PROJECT_NAME

def main_program():
    """
    Demonstrates how to call log normalization for a project.
    Assumes there is an existing project with already-loaded pilot log, pilot trajectory,
    active log (LWD), and active trajectory.

    This call will apply the requested tvdss_offset to the (vertically straightened)
    pilot log, then return a structure having the three fields Factor Drive needs
    you to pass in.
    """

    initialize_drive_api(drive_host=DRIVE_HOST, port=PORT,
                         token=TOKEN, secure=SECURE)

    # TODO you would upload your very latest LWD and trajectory here
    # using put_lwd_samples() and put_trajectory() as in example.py
    # (But you would not kick off a job, of course)
    #
    # put_lwd_samples(project_name, mnemonic=mnemonic, samples=samples)
    # put_trajectory(project_name, md_incl_azi_triples)

    # Ask Drive to calculate the log normalization parameters. This
    # call will use the tvdss_offset you pass in and return it
    # with the fit_params.

    tvdss_offset = 142
    fit_params = log_normalization_fit_params(project_name, tvdss_offset)
    print(fit_params)

    # now let's update the well with the new fit_params:
    update_pilot_well(project_name, fit_params)

    # let's just see that we did update the fit params
    pilot_well = get_pilot_well(project_name)
    fit_params_2 = pilot_well['fit_params']
    print(json.dumps(fit_params_2))

    assert fit_params_2 == fit_params

main_program()
