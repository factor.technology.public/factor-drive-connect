# Copyright © 2021 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
This is an example program that you can use as a starter template to script Factor Drive from
StarSteer. There's a companion module named `drive_api.py` you should not need to modify.

This example illustrates updating the tvdss offset for the pilot well.
"""
# pylint: disable=global-statement,invalid-name

from drive_config import TOKEN
from drive_config import DRIVE_HOST
from drive_config import PORT
from drive_config import SECURE
from drive_config import SCOPE

from drive_api import initialize_drive_api
from drive_api import get_pilot_well
from drive_api import update_pilot_well

PROJECT_NAME = 'hw DJ G 2N'

# Here are variables you'll set for each project
project_name = PROJECT_NAME
tvdss_offset = 0

def main_program():
    """
    Main program for the example, which illustrates updating the tvdss_offset for the pilot well.
    """

    # The beginning of the main program.
    # You must initialize the drive_api module with the host name and your API token.
    initialize_drive_api(drive_host=DRIVE_HOST,
                         port=PORT,
                         token=TOKEN,
                         secure=SECURE)

    pilot_well = get_pilot_well(SCOPE, project_name)

    if len(pilot_well.keys()):
        fit_params = pilot_well['fit_params']
        fit_params['tvdss_offset'] = tvdss_offset

        update_pilot_well(SCOPE, PROJECT_NAME, fit_params)

main_program()
