# Copyright © 2021 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
This is an example program that you can use as a starter template to script Factor Drive from
StarSteer. There's a companion module named `drive_api.py` you should not need to modify.

This example creates a project, a pilot well, and a pilot log curve with zero datum elevation.

Prerequisites:
  Create a valid user by logging in to the Factor client application.
  Set the constant 'USERNAME' (below) to that user's name.
  In the client app, create a JWT token for that user and assign the single-quoted string to
  constant 'TOKEN' (below).
  Create a user group 'terraguidance' via the Factor client. If a different name is used,
  change assignment to variable 'group_name' (below) to match.
"""
# pylint: disable=global-statement,invalid-name

import json

from drive_api import initialize_drive_api
from drive_api import create_project
from drive_api import delete_project
from drive_api import create_pilot_well
from drive_api import put_pilot_log
from drive_api import apparent_dip_to_true_dip_and_strike

# Set constants we'll use.
# Use your own API token here:

# pylint: disable=line-too-long
USERNAME = 'brad.harper'
TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzUyMDQzNjAsImF1ZCI6Ii9hcGkvZHJpdmUvdjEiLCJpc3MiOiJGYWN0b3IgVGVjaG5vbG9neSIsInN1YiI6ImJyYWQuaGFycGVyIiwianRpIjoiNTNmMjhjYmMtNmNiMS00NzI3LThmOGYtYzJkM2Q4MzM1YmZlIn0.ebSxJfNP5R0_9MKMWXDtMEAc0Ntsw7lxxd8pCNCTMJc'
# pylint: enable=line-too-long
PROJECT_NAME = 'hw DJ G 2N'
DRIVE_HOST = 'localhost'
PORT=3000
SECURE=False

# These are constants for the purpose of this program. You won't need to copy them.
PROJECT_FILE_NAME = 'hw-DJ-G-2N.project.json'
PILOT_LOG_FILE_NAME = 'hw-DJ-G-2N--pilot.json'
PILOT_WAYPOINTS_FILE_NAME = 'hw-DJ-G-2N--pilot.trajectory.json'
LWD_FILE_NAME = 'hw-DJ-G-2N.lwd.json'
LWD_WAYPOINTS_FILE_NAME = 'hw-DJ-G-2N.trajectory.json'

# Here are variables you'll set for each project
scope = f'{USERNAME}'
group_name = 'terraguidance'
project_name = 'hw-DJ-G-2N'
active_datum_elevation = 4755
pilot_datum_elevation = 4745
apparent_dip_deg = 92
vs_azimuth_deg = 285.64

def load_log(file_name):
    """
    Load the log curve data from a named file.
    Return a list of [md, gr] pairs.
    """
    with open(file_name, encoding='utf-8') as file:
        log_dict = json.load(file)

        mnemonic = log_dict['mnemonic']
        samples = log_dict['samples']

        return (samples, mnemonic)


def load_waypoints(file_name):
    """
    Load the trajectory waypoints from a named file.
    Return a list of [md, inc, azi] triples.
    """
    with open(file_name, encoding='utf-8') as file:
        waypoints = json.load(file)

        md_incl_azi_triples = [[triple['md'], triple['incl'], triple['azi']]
                               for triple in waypoints]
        return md_incl_azi_triples


def load_project(file_name):
    """
    Load the project from the named file.
    """
    with open(file_name, encoding='utf-8') as file:
        project_dict = json.load(file)

        return project_dict

def main_program():
    """Main program for the example."""

    # The beginning of the main program.
    # You must initialize the drive_api module with the host name and your API token.
    initialize_drive_api(
      drive_host=DRIVE_HOST,
      port=PORT,
      token=TOKEN,
      secure=SECURE)

    # Use the apparent dip to compute the (dip, strike) we need to mass to Drive.
    dip, strike = apparent_dip_to_true_dip_and_strike(
        apparent_dip_deg, vs_azimuth_deg)

    print('computed dip =', dip)
    print('computed strike = ', strike)

    # the complementary script 'example.py' fails, if the dip/strike computed here are used. the
    # script succeeds if dip/strike match values taken from the adjacent 'hw-DJ-G-2N.project.json'
    # file.
    project = {
        'name': project_name,
        'dip_constant': 0,
        'strike_constant': 180,
        'vs_azimuth': vs_azimuth_deg,
        'active_datum_elevation': active_datum_elevation,
        'is_from_tot': True,
        'md_marker': 8083.40,

        'fault_num_total': 2,
        'fault_throw_min': 4,
        'fault_throw_max': 40,
        'fault_up_weight': 0.5,

        'dip_sigma': 2,
        'delta_spatial': 0.5,
        'log_distribution': 'normal',
        'log_sigma': 25,

        # No need to modify these for the near future:
        'dip_type': 'constants',
        'md_first_to_compute': 8083.40,
        'md_last_to_compute': 17973,
        'interval': 12,
        'discard_all_but_last_log_segment': False,
        'cull_factor': 0.003,

        # Historical artifacts. Ignore:
        'sigmaDipCurv': 0.01,
        'inclination_sigma': 0.01,
        'sigma_md': 0.04
    }

    # the complementaty script 'example.py' works, if the project is loaded from the adjacent
    # 'hw-DJ-G-2N.project.json' file.

    # project = load_project(file_name=PROJECT_FILE_NAME)

    # delete the project, if it exists.
    delete_project(scope, project_name)

    # create the project.
    create_project(scope, project)

    # create the type well.

    create_pilot_well(scope,
                      project_name,
                      'hw-DJ-G-2N--pilot',
                      datum_elevation=pilot_datum_elevation,
                      start_md=0)

    # add data/trajectory for the type well.
    samples, mnemonic = load_log(file_name=PILOT_LOG_FILE_NAME)
    pilot_waypoints = load_waypoints(file_name=PILOT_WAYPOINTS_FILE_NAME)
    trajectory = {'waypoints': pilot_waypoints}

    put_pilot_log(scope,
                  project_name,
                  mnemonic,
                  samples,
                  trajectory,
                  datum_elevation=pilot_datum_elevation)

main_program()

# separate code run subsequently would add log/trajectory for the active well
# and run the job.
#
# samples, mnemonic = load_log(LWD_FILE_NAME)
# lwd_waypoints = load_waypoints(LWD_WAYPOINTS_FILE_NAME)
#
# put_lwd_samples_log(...)
# put_trajectory(...)
#
# run_job(PROJECT_NAME)
