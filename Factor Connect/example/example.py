# Copyright © 2021 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
This is an example program that you can use as a starter template to script Factor Drive from
StarSteer. There's a companion module named `drive_api.py` you should not need to modify.

This program simulates a growing interpretation. Every 5 minutes it adds another 1000 feet
of LWD and trajectory. In real life you'd get that data from StarSteer. In this simulation
we load it from JSON files.

This program doesn't set up a Factor Drive project from scratch. It assumes you have already
created the project 'hw-DJ-G-2N', that you've collected data at least down to the top of
target, and that you've set the whole project up (loading LAS and trajectory, aligned
and scaled the type log to fit the LWD). That's a manual process you do in the Factor Drive
web app.

See the module `create-project-and-pilot-well.py`; its pre-requisites are shared by this module:

  Create a valid user by logging in to the Factor client application.
  Set the constant 'USERNAME' (below) to that user's name.
  In the client app, create a JWT token for that user and assign the single-quoted string to
  constant 'TOKEN' (below).
  Create a user group 'terraguidance' via the Factor client. If a different name is used,
  change assignment to variable 'group_name' (below) to match.

Once that's done, you're ready to grow the lateral with a program like this.

This program starts a timer thread that runs every 20 seconds. Each time it runs, it retrieves
the current computed structure, and the status of any processing that's running, from Factor Drive.
Whenever another five minutes elapses, it loads 1000 more feet of new LAS and trajectory data,
as if someone had loaded more data into SS. Then, when we see the longer data, we submit
it to Factor Drive and run a job to update the computation.
"""
# pylint: disable=global-statement,invalid-name

import json
import time

from threading import Timer

from drive_api import DriveAPIException
from drive_api import initialize_drive_api
from drive_api import get_mpe
from drive_api import get_job_status
from drive_api import put_lwd_samples
from drive_api import put_trajectory
from drive_api import run_job

# Set constants we'll use.
# Use your own API token here:

# pylint: disable=line-too-long
USERNAME = 'brad.harper'
TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzUyMDQzNjAsImF1ZCI6Ii9hcGkvZHJpdmUvdjEiLCJpc3MiOiJGYWN0b3IgVGVjaG5vbG9neSIsInN1YiI6ImJyYWQuaGFycGVyIiwianRpIjoiNTNmMjhjYmMtNmNiMS00NzI3LThmOGYtYzJkM2Q4MzM1YmZlIn0.ebSxJfNP5R0_9MKMWXDtMEAc0Ntsw7lxxd8pCNCTMJc'
# pylint: enable=line-too-long
PROJECT_NAME = 'hw-DJ-G-2N'
DRIVE_HOST = 'localhost'
PORT=3000
SECURE=False

scope = f'{USERNAME}'
group_name = 'terraguidance'
project_name = PROJECT_NAME
timer = None

# The next two functions load the example LWD and trajectory data from files.
# In practice you'll get this data from StarSteer.
def load_lwd_samples(md_limit):
    """
    Load the example LWD data from the first sample up to `md_limit`.
    Return a list of [md, gr] pairs.
    """
    lwd_file_name = 'hw-DJ-G-2N.lwd.json'
    with open(lwd_file_name) as lwd_file:
        lwd_dict = json.load(lwd_file)

        mnemonic = lwd_dict['mnemonic']
        samples_orig = lwd_dict['samples']

        samples_limited = list(
            filter(lambda pair: pair[0] < md_limit, samples_orig))
        return (samples_limited, mnemonic)


def load_lwd_waypoints(md_limit):
    """
    Load the example trajectory from the first survey up to `md_limit`
    Return a list of [md, inc, azi] triples.
    """
    trajectory_file_name = 'hw-DJ-G-2N.trajectory.json'
    with open(trajectory_file_name) as trajectory_file:
        waypoints_orig = json.load(trajectory_file)

        waypoints_limited = list(
            filter(lambda waypoint: waypoint['md'] < md_limit, waypoints_orig))

        return waypoints_limited

# These are some constants defining how we'll march through the example data files.
# Every five minutes we'll add 1000 feet. In practice, when you're getting this data in Star Steer,
# you would only update Factor Drive when the user has extended the LWD and the trajectory.
# Here, every five minutes like clockwork we're going to extend them.
md_start = 9000
md_step = 1000
md_stop = 18000
minutes_between_jobs = 5
timer_interval = 20   # secs

# These variables track our progress through the simulation.
seconds_last = 0
minutes_since_last = minutes_between_jobs
md_to_load = md_start
prev_samples_len = 0
prev_trajectory_len = 0

def sync_with_drive():
    """
    This is the function we call repeatedly in the timer.
    """
    print()

    # get the current structure data from Drive:
    print('retrieve mpe')
    try:
        mpe = get_mpe(scope, project_name)
        # Ordinarily you'll display this computed structure in SS. Here, just print a few elements.
        print(mpe[0:9], '...')
    except DriveAPIException as e:
        print(f'get_mpe failed: {e}')

    # As a job is processing you can get progress info from the status.
    try:
        status = get_job_status(scope, project_name)
        print(status)
    except DriveAPIException as e:
        print(f'get_job_status failed: {e}')

    global minutes_since_last, seconds_last
    global md_to_load, md_step
    global prev_samples_len, prev_trajectory_len
    global timer

    seconds_now = time.time()
    minutes_since_last = (seconds_now - seconds_last) / 60

    samples, mnemonic = ([], None)
    waypoints = []

    # Get the latest LWD and trajectory from Star Steer.
    # Here, we simulate doing that by loading another 1000 feet every five minutes.
    if minutes_since_last >= minutes_between_jobs and md_to_load <= md_stop:

        # get latest data from fake star steer:
        samples, mnemonic = load_lwd_samples(md_to_load)
        waypoints = load_lwd_waypoints(md_to_load)

    # Check whether the `samples` and `md_incl_azi_triples` are longer than the previous ones.
    # If they are longer, submit them to Factor Drive and run a job.
    if len(samples) > prev_samples_len and len(waypoints) > prev_trajectory_len:
        prev_samples_len = len(samples)
        prev_trajectory_len = len(waypoints)
        try:
            # Send the LWD
            print('send LWD')
            put_lwd_samples(scope, project_name, mnemonic=mnemonic, samples=samples)

            # Send the trajectory
            print('send trajectory')
            put_trajectory(scope, project_name, waypoints)

            # Kick off processing of that new data
            # run_job returns as soon as the job starts. The job runs remotely, and we check
            # its status every time through the loop using `get_job_status`
            print('run job')
            run_job(scope, project_name)

            # update the md and time counters.
            md_to_load = md_to_load + md_step
            seconds_last = seconds_now

        except DriveAPIException as e:
            print(f'Failed updating Factor Drive with new data: {e}')

    # Do it all again in 20 secs
    timer = Timer(timer_interval, sync_with_drive)
    timer.start()


# The beginning of the main program.
# You must initialize the ../drive_api module with the host name and your API token.
initialize_drive_api(
    drive_host=DRIVE_HOST,
    port=PORT,
    secure=SECURE,
    token=TOKEN)

def main_program():
    """Create or update a project"""
    global timer
    timer = Timer(timer_interval, sync_with_drive)
    timer.start()
    print('timer started')

main_program()
