# pylint: disable=import-error,undefined-variable,bare-except
# pylint: disable=invalid-name,missing-module-docstring,line-too-long
from importlib import reload
from factor_connect import sync_with_drive

import drive_config
import drive_api
import factor_connect

reload(drive_config)
reload(drive_api)
reload(factor_connect)

config = drive_config.DriveConfig(
    token = token,
    host = host,
    scope = scope
)

well = Wells[ActiveLateralName]

try:
    unsminterp = well.Interpretations["Factor - Unsmoothed"]
except:
    Interpretations.create(ActiveLateralName, "Factor - Unsmoothed",
                           well.Interpretations[ActiveInterpretationName])
    unsminterp = well.Interpretations["Factor - Unsmoothed"]

sync_with_drive(config, project_name, well, gammaLog, unsminterp)
