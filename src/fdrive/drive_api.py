# -*- coding: utf-8 -*-
# Copyright © 2024 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
A helper interface to the Factor Drive® API
"""

# TODO put_lwd, put_pilot_log, get_trajectory

# pylint: disable=global-statement,invalid-name,redefined-outer-name,too-many-arguments
from urllib.parse import quote
import collections
import math
import json
import http.client

# Your API token
token = ''
port = 443

# Your Factor Drive hostname:
drive_host = ''
secure = True

RAD_PER_DEG = math.pi / 180


class DriveAPIException (Exception):
    """A general exception thrown when any API call fails"""


class _get_json():
    def __init__(self, resource_id):
        self.resource_id = resource_id

    def __enter__(self):
        headers = {'authorization': f'Bearer {token}'}
        conn = (http.client.HTTPSConnection(drive_host, port)
                if secure else http.client.HTTPConnection(drive_host, port))
        conn.request('GET', self.resource_id, None, headers)
        response = conn.getresponse()
        status = response.status
        if 300 > status >= 200:
            # The request succeeded
            # The response is a JSON object
            response_str = response.read().decode()
            response_dict = json.JSONDecoder().decode(response_str)
            return response_dict

        reason = response.reason
        raise DriveAPIException(
            f'request failed with status {status}: {reason}')

    def __exit__(self, _type, _value, _traceback):
        # Exception handling here
        self.resource_id = None


def _delete_resource(resource_id):
    _send_json(resource_id, '', 'DELETE')


def _assert_valid():
    if drive_host == '' or drive_host is None:
        raise DriveAPIException(
            'drive_host must not be empty. Call initialize_drive_api().')
    if token == '' or token is None:
        raise DriveAPIException(
            'API token must not be empty.')


def _send_json(resource_id, body_str, verb):
    body_b = bytes(body_str, 'utf-8')
    headers = {
        'authorization': f'Bearer {token}',
        'content-type': 'application/json',
        'content-length': len(body_b)
    }

    conn = (http.client.HTTPSConnection(drive_host, port, timeout=600)
            if secure else http.client.HTTPConnection(drive_host, port, timeout=600))

    conn.request(verb, resource_id, body=body_b, headers=headers)
    response = conn.getresponse()
    status = response.status
    missing_but_ok = verb == 'DELETE' and status == 404
    is_success = (300 > status >= 200) or missing_but_ok

    if is_success:
        response_str = response.read().decode()
        if response.getheader('Content-type', 'text/plain').startswith('application/json'):
            response_dict = json.JSONDecoder().decode(
                response_str) if response_str and len(response_str) > 0 else {}
            return response_dict
        return None

    reason = response.reason

    # The response may be a JSON object
    response_str = response.read().decode()

    if response.getheader('Content-type', 'text/plain').startswith('application/json'):
        response_dict = json.JSONDecoder().decode(
            response_str) if response_str and len(response_str) > 0 else {}
        error = response_dict.get('error', 'That is all we know.')
        message = response_dict.get('message', '')
        nested_message = response_dict.get('nestedMessage', '')
    else:
        error = response_str
        message = ''
        nested_message = ''

    raise DriveAPIException(
        f'request failed with status {status}: {reason}. {error} {message} {nested_message}')


def _put_json(resource_id, body_str):
    _send_json(resource_id, body_str, 'PUT')


def _post_json(resource_id, body_str):
    return _send_json(resource_id, body_str, 'POST')


def _job_options_from_project(project_name, project_dict):
    is_from_tot = project_dict.get('is_from_tot', False)
    md_first_to_compute = project_dict.get('md_first_to_compute')

    if is_from_tot is True:
        md_marker = project_dict['md_marker']
    else:
        if md_first_to_compute >= 0:
            md_marker = md_first_to_compute
        else:
            raise DriveAPIException((
                "inconsistent job options: 'is_from_tot' is 'False' AND 'md_first_to_compute'/n" +
                " is  undefined. either set 'is_from_tot' to 'True' to use the value of/n" +
                "'md_marker'  or define a value for 'md_first_to_compute'."
            ))

    job_options = {
        "projectName": project_name,
        "dip": project_dict.get('dip_constant'),
        "strike": project_dict.get('strike_constant'),
        "dipType": project_dict.get('dip_type'),
        "mdFirst": project_dict.get('md_first_to_compute'),
        "mdLast": 99999,
        "vsazi": project_dict.get('vs_azimuth'),
        "datum": project_dict.get('active_datum_elevation'),
        "sigmaLog": project_dict.get('log_sigma'),
        "sigmaDip": project_dict.get('dip_sigma'),
        "sigmaDipCurv": 0.01,
        "sigmaIncl": 0.01,
        "faultNumTotal": project_dict.get('fault_num_total', 0),
        "faultThrowMin": project_dict.get('fault_throw_min', 5),
        "faultThrowMax": project_dict.get('fault_throw_max', 100),
        "faultUpWeight": project_dict.get('fault_up_weight', 0.5),
        "mdMarker": md_marker,
        "interval": project_dict.get('interval'),
        "sigmaMD": 0.01,
        "deltaSpatial": project_dict.get('delta_spatial'),
        "discardAllButLastLogSegment": project_dict.get('discard_all_but_last_log_segment'),
        "logDistribution": project_dict.get('log_distribution') or 'normal',
        "cullFactor": project_dict.get('cull_factor', 0.003)
    }

    return job_options


def _trajectory_to_waypoints(trajectory):

    # support legacy code passing 'waypoints'
    if (trajectory
            and isinstance(trajectory, collections.abc.Mapping)
            and trajectory.get('waypoints', None)):
        surveys = trajectory['waypoints']
    elif trajectory and isinstance(trajectory, collections.abc.Sequence):
        surveys = trajectory
    elif trajectory:
        raise RuntimeError('trajectory has unexpected shape')
    else:
        surveys = trajectory

    waypoints = None
    if surveys:
        waypoints = _surveys_to_waypoints(surveys)
    return waypoints


def _surveys_to_waypoints(surveys):

    waypoints = []
    if len(surveys) > 0:
        first_survey = surveys[0]
        if isinstance(first_survey, collections.abc.Mapping):
            waypoints = surveys
        elif isinstance(first_survey, collections.abc.Sequence):
            waypoints = [{
                'md': triple[0],
                'incl': triple[1],
                'azi': triple[2]
            } for triple in surveys]
    return waypoints


def _get_pilot_well_id(scope, project_name):
    """Get the pilot well id of [the first pilot well]"""
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells'

    with _get_json(resource_id) as pilot_wells_dict:
        if len(pilot_wells_dict.keys()):
            key = list(pilot_wells_dict.keys())[0]
            return key

        return None


def _sample_interval_from_samples(samples):
    if not samples or len(samples) < 2:
        return 1.0
    md_0, _v0 = samples[0]
    md_1, _v1 = samples[1]
    return abs(md_1 - md_0)


def apparent_dip_to_true_dip_and_strike(apparent_dip_deg, vs_azimuth_deg):
    """
    A convenience function to help populate project properties. Returns (dip, strike)

    The project properties for dip and strike allow you to specify any possible dip and strike.
    However, most of the time, geologists only know what the apparent dip is. We assume the
    vs_azimuth points along the dip direction. Then, by Drive's convention, the strike is 90°
    left of that. Use this function to make sure you honor the Drive convention when you're
    converting apparent dip to true dip and strike in the project parameters.
    """
    strike_deg = vs_azimuth_deg - 90
    strike_rad = RAD_PER_DEG * strike_deg
    heading_rad = RAD_PER_DEG * vs_azimuth_deg

    denom = math.sin(heading_rad - strike_rad)

    # If you're headed in the strike direction, we can't derive a true dip.
    # It could be anything.
    if abs(denom) < 0.1:
        return (0, strike_deg)

    dip_true_deg = math.atan(
        math.tan((apparent_dip_deg - 90) * RAD_PER_DEG) / denom) / RAD_PER_DEG

    return (dip_true_deg, strike_deg)


def get_mpe(scope, project_name):
    """Return a list of (md, tvdss) pairs representing the computed structure."""
    _assert_valid()

    # Build the URL that will retrieve the "mpe", that means "most probable explanation".
    # It's the computed structure.
    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/job/results/mpe'

    with _get_json(resource_id) as mpe_dict:
        # We've read the JSON into a Python dict. Each key of the dict is an MD.
        # The key is formatted as a string with leading zeroes.

        # There's a special "MD" in every response that we want to ignore.
        # It's not really an MD value, it's the string '.......'
        # The next line filters that out of the list of keys
        md_keys = sorted(
            list(filter(lambda key: key != '.......', mpe_dict.keys())))

        # We have the MDs in ascending order.
        # Let's now make a list of (md, tvdss) pairs (tuples):

        sorted_md_depth_pairs = list(map(
            lambda md_key: (float(md_key.lstrip('0') or '0'),
                            mpe_dict[md_key].get('formation_tvd')), md_keys))
    return sorted_md_depth_pairs


def get_job_status(scope, project_name):
    """
    Return a dict like:
      {
        "type": "update",
        "firstForwardMDInLog": 8566,
        "lastForwardMDInLog": 18570,
        "lastReverseMDInLog": 8566,
        "firstSuccessTime": "2021-05-20T14:25:32.000Z",
        "lastSuccessTime": "2021-05-20T14:41:07.000Z",
        "lastDirectionInLog": "completed",
        "firstMDInJob": 8566,
        "lastMDInJob": 18570
      }
      """
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/job/status'

    with _get_json(resource_id) as status_dict:
        return status_dict


def list_projects():
    """Return a list of all projects the token gives read permission to."""
    _assert_valid()
    resource_id = '/api/v2/projects/prelim-summaries'

    with _get_json(resource_id) as index_list:
        return index_list


def get_project(scope, project_name):
    """Get the project object as a dict."""
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}'

    with _get_json(resource_id) as project_dict:
        return project_dict


def project_exists(scope, project_name):
    """Return True if the project exists with the given name."""
    _assert_valid()

    try:
        get_project(scope, project_name)
    except DriveAPIException:
        return False

    return True


def create_project(scope, project):
    """Create a Factor project."""
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects'

    _post_json(resource_id, json.dumps(project))


def update_project(scope, project):
    """Update an existing Factor project."""
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects/{quote(project["name"])}'

    _put_json(resource_id, json.dumps(project))


def put_project(scope, project):
    """Alias for update_project"""
    update_project(scope, project)


def delete_project(scope, project_name):
    """Remove the named project."""
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}'

    _delete_resource(resource_id)


def get_pilot_well(scope, project_name):
    """Get the pilot well object as a dict [from the first pilot well]"""
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells'

    with _get_json(resource_id) as pilot_wells_dict:
        if len(pilot_wells_dict.keys()):
            key = list(pilot_wells_dict.keys())[0]
            pilot_well_dict = pilot_wells_dict[key]
            pilot_well_dict['id'] = key
            return pilot_well_dict

        return None


def create_pilot_well(scope,
                      project_name,
                      well_name,
                      datum_elevation=0,
                      start_md=0):
    """Create pilot well."""
    _assert_valid()

    fit_params = {'grScale': 1,
                  'grOffset': 0,
                  'tvdssOffset': 0}
    pilot_well_dict = {
        "name": well_name,
        "datum_elevation": datum_elevation,
        "start_md": start_md,
        "fit_params": fit_params
    }

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells'

    _post_json(resource_id, json.dumps(pilot_well_dict))


def update_pilot_well(scope,
                      project_name,
                      fit_params,
                      datum_elevation=None,
                      start_md=None):
    """Update pilot well. Assumes one pilot."""
    _assert_valid()

    well_id = _get_pilot_well_id(scope, project_name)

    if well_id is not None:
        resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells/{well_id}'

        with _get_json(resource_id) as pilot_well:
            pilot_well['fit_params'] = fit_params

            if datum_elevation is not None:
                pilot_well['datum_elevation'] = datum_elevation

            if start_md is not None:
                pilot_well['start_md'] = start_md

            _put_json(resource_id, json.dumps(pilot_well))

    return False


def put_pilot_well(scope,
                   project_name,
                   fit_params,
                   datum_elevation=None,
                   start_md=None):
    """Alias for update_pilot_well"""
    update_pilot_well(scope, project_name, fit_params,
                      datum_elevation, start_md)


def get_pilot_log(scope, project_name):
    """Get the pilot log for the named project. Assumes a single pilot well."""
    _assert_valid()

    well_id = _get_pilot_well_id(scope, project_name)

    if well_id is not None:
        resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells/{well_id}'

        with _get_json(resource_id) as pilot_log:
            return pilot_log

    return None


def put_pilot_log(scope,
                  project_name,
                  mnemonic='GR',
                  samples=None,
                  trajectory=None,
                  datum_elevation=0):
    """Set a pilot log for the named project."""
    _assert_valid()

    sample_interval = _sample_interval_from_samples(samples)
    wells_resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells'
    with _get_json(wells_resource_id) as wells:
        well_id = list(wells.keys())[0]

        pilot_log = {
            'mnemonic': mnemonic,
            'index_uom': 'ft',
            'value_uom': 'gAPI',
            'is_periodic': True,
            'sample_interval': sample_interval,
            'index_domain': 'MD',
            'orig_index_uom': '',
            'orig_value_uom': '',
            'samples': samples or []
        }

        request = {
            'log_curve': pilot_log,
            'datum_elevation': datum_elevation
        }

        waypoints = _trajectory_to_waypoints(trajectory)
        if waypoints:
            request['trajectory'] = {'waypoints': waypoints}

        logs_resource_id = (
            f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells/{well_id}/logs/GR')
        _post_json(logs_resource_id, json.dumps(request))


def get_lwd(scope, project_name):
    """Get the current LWD for the named project."""
    _assert_valid()

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/active-well/logs/GR'

    with _get_json(resource_id) as lwd:
        return lwd


def put_lwd(scope,
            project_name,
            lwd):
    """Set the current LWD for the project"""
    _assert_valid()
    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/active-well/logs/GR'
    _put_json(resource_id, json.dumps(lwd))


def put_lwd_samples(scope,
                    project_name,
                    mnemonic='GR',
                    samples=None):
    """Set the current LWD for the project"""
    _assert_valid()

    lwd = {
        'mnemonic': mnemonic,
        'index_uom': 'ft',
        'value_uom': 'gAPI',
        'is_periodic': True,
        'sample_interval': _sample_interval_from_samples(samples),
        'index_domain': 'MD',
        'orig_index_uom': '',
        'orig_value_uom': '',
        'samples': samples or []
    }

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/active-well/logs/GR'
    _put_json(resource_id, json.dumps(lwd))


def put_trajectory(scope, project_name, trajectory):
    """Set the current trajectory for the project"""
    _assert_valid()

    waypoints = _trajectory_to_waypoints(trajectory)

    trajectory = {'waypoints': waypoints}

    resource_id = (
        f'/api/v2/{scope}/projects/{quote(project_name)}/active-well/trajectories/measured')
    _put_json(resource_id, json.dumps(trajectory))


def log_normalization_fit_params(
        scope,
        project_name,
        tvdss_offset,
        is_offset_fixed=True,
        autofit_window_height=200):
    """Get the pilot well object as a dict [from the first pilot well]"""
    _assert_valid()

    well_id = _get_pilot_well_id(scope, project_name)

    if well_id is not None:
        resource_id = (
            f'/api/v2/{scope}/projects/{quote(project_name)}/pilot-wells/{well_id}/fit-params')
        request_dict = {
            'tvdss_offset': tvdss_offset,
            'is_offset_fixed': is_offset_fixed,
            'autofit_window_height': autofit_window_height
        }

        fit_params = _post_json(resource_id, json.dumps(request_dict))
        return fit_params

    return None


def run_job(scope, project_name):
    """
    Retrieve the project, build job options from that, and
    submit the job to the server.
    """
    _assert_valid()

    project_dict = get_project(scope, project_name)

    resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/job'
    job_options = _job_options_from_project(project_name, project_dict)
    _post_json(resource_id, json.dumps(job_options))


def reset_job(scope, project_name):
    """Reset [stop] the job [if any] for the named project."""
    _assert_valid()

    if project_exists(scope, project_name):
        resource_id = f'/api/v2/{scope}/projects/{quote(project_name)}/job'
        _put_json(resource_id, json.dumps({}))


def initialize_drive_api(
    drive_host=None,
    token=None,
    secure=None,
    port=None
):
    """Call this function once before any other calls, to set the server name and the API token."""
    module = globals()
    if drive_host:
        module['drive_host'] = drive_host
    if token:
        module['token'] = token
    if secure is not None:
        module['secure'] = secure
    if port:
        module['port'] = port
