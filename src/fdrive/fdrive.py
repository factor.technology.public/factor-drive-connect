#!/usr/bin/env python3
# Copyright © 2024 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
Command line interface for Factor Drive

This program provides a command line interface for managing projects in Factor Drive. 
You can create, delete, and update projects, as well as manage LWD data and trajectory 
data for each project.

Usage:
    python drive.py COMMAND

Commands:
    create-project: Creates a new project.
    delete-project: Deletes a project.
    get-project: Gets information about a project.
    put-project: Updates a project.
    get-lwd: Gets LWD data for a project.
    put-lwd: Puts LWD data for a project.
    get-trajectory: Gets trajectory data for a project.
    put-trajectory: Puts trajectory data for a project.
    run-job: Runs a job for a project.
    reset-job: Resets a job for a project.

For more information about a command, run `python drive.py COMMAND --help`.
"""
import os
import json
import fire
from .config import get_config
from . import drive_api

def _maybe_json_file(input_string):
    if input_string.startswith('@'):
        # The input is a file path
        file_path = input_string[1:]  # Remove the '@' prefix
        file_path = os.path.expanduser(file_path)  # Expand '~' to the home directory
        with open(file_path, 'r', encoding='utf-8') as file:
            return file.read()
    else:
        # The input is a regular string
        return input_string
class Drive(object):
    """
    A command line interface for Factor Drive. 
    
    
    This program provides a command line interface for managing projects in Factor Drive. 
    You can create, delete, and update projects, as well as manage LWD data and trajectory 
    data for each project.

    You must edit the .driverc.yaml file to set the token for 
    your Factor Drive instance. Obtain your token at https://drive.factor.technology/jwt.
    The file can contain a single line like:
    
    token: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    """

    def list_projects(self):
        """
        Lists all projects permitted to the current user. 
        """
        dict_projects = drive_api.list_projects()
        return json.dumps(dict_projects)

    def create_project(self, project_id, project):
        """
        Creates a new project. 
        
        Args:
            project_id: The ID of the project to create.
            project: The project to create. This should be a JSON string or file.
        """
        [project_scope, project_name] = project_id.split('/')
        json_project = _maybe_json_file(project)
        dict_project = json.JSONDecoder().decode(json_project)
        dict_project['name'] = project_name
        dict_project['scope'] = project_scope
        drive_api.create_project(project_scope, dict_project)

    def delete_project(self, project_id):
        """Deletes a project. """
        [project_scope, project_name] = project_id.split('/')
        drive_api.delete_project(project_scope, project_name)

    def get_project(self, project_id):
        """Gets a project. """
        [project_scope, project_name] = project_id.split('/')
        dict_project = drive_api.get_project(project_scope, project_name)
        return json.dumps(dict_project)

    def put_project(self, project):
        """
        Updates an existing project. 
        """
        json_project = _maybe_json_file(project)
        dict_project = json.JSONDecoder().decode(json_project)
        project_scope = dict_project['scope']
        drive_api.put_project(project_scope, dict_project)

    def get_lwd(self, project_id):
        """
        Gets LWD data for a project. 
        """
        [project_scope, project_name] = project_id.split('/')
        dict_lwd = drive_api.get_lwd(project_scope, project_name)
        return json.dumps(dict_lwd)

    def put_lwd_samples(self, project_id: str, mnemonic: str, samples: str):
        """
        Puts LWD data for a project.
        
        Args:
            project_id: The project ID 'scope/name'.
            mnemonic: The mnemonic to display for LWD curve.
            samples: The samples of the LWD curve.
                Either a literal JSON array of [depth, value] pairs like
                [[9000, 100.6], [9001, 102.1], [9002, 99.7]],
                or a file path prefixed with '@' like '@/path/to/file.json' containing the json.
        """
        [project_scope, project_name] = project_id.split('/')
        json_samples = _maybe_json_file(samples)
        dict_samples = json.loads(json_samples)
        drive_api.put_lwd_samples(project_scope, project_name, mnemonic, dict_samples)

    def get_mpe(self, project_id):
        """
        Gets the MPE (the computed horizon) for a project.
        """
        [project_scope, project_name] = project_id.split('/')
        dict_mpe = drive_api.get_mpe(project_scope, project_name)
        return json.dumps(dict_mpe)

    def get_job_status(self, project_id):
        """
        Gets the job status for a project. 
        
        """
        [project_scope, project_name] = project_id.split('/')
        dict_mpe = drive_api.get_job_status(project_scope, project_name)
        return json.dumps(dict_mpe)

    def create_pilot_well(self,
                          project_id,
                          well_name,
                          datum_elevation=0,
                          start_md=0):
        """Create pilot well."""
        [project_scope, project_name] = project_id.split('/')
        drive_api.create_pilot_well(
            project_scope, project_name, well_name, datum_elevation, start_md)

    def put_pilot_well(self,
                       project_id,
                       fit_params,
                       datum_elevation=None,
                       start_md=None):
        """
        Update the pilot well for the project. 
        """
        fit_params_dict = json.JSONDecoder().decode(fit_params)
        [project_scope, project_name] = project_id.split('/')
        drive_api.put_pilot_well(
            project_scope, project_name, fit_params_dict, datum_elevation, start_md)

    def get_pilot_log(self, project_id):
        """
        Gets the pilot log for a project.
        """
        [project_scope, project_name] = project_id.split('/')
        dict_pilot_log = drive_api.get_pilot_log(project_scope, project_name)
        return json.dumps(dict_pilot_log)

    def put_pilot_log(self,
                      project_id,
                      samples,
                      mnemonic='GR',
                      trajectory=None,
                      datum_elevation=0):
        """
        Update the pilot log for a project. 
        """
        [project_scope, project_name] = project_id.split('/')
        json_samples = _maybe_json_file(samples)
        list_samples = json.loads(s=json_samples)
        dict_trajectory = None
        if trajectory:
            json_trajectory = _maybe_json_file(trajectory)
            dict_trajectory = json.loads(s=json_trajectory)
        drive_api.put_pilot_log(project_scope, project_name,
                                mnemonic, list_samples, dict_trajectory, datum_elevation)

    # def get_trajectory(self, project):
    #     """Gets trajectory data for a project.
    #        """
    #     [project_scope, project_name] = project.split('/')
    #     drive_api.get_trajectory(project_scope, project_name)

    def put_trajectory(self, project_id, trajectory):
        """
        Puts trajectory data for a project. 
         
        """
        [project_scope, project_name] = project_id.split('/')
        json_trajectory = _maybe_json_file(trajectory)
        dict_trajectory = json.loads(json_trajectory)
        drive_api.put_trajectory(project_scope, project_name, dict_trajectory)

    def run_job(self, project_id):
        """Runs a job for a project. """
        [project_scope, project_name] = project_id.split('/')
        drive_api.run_job(project_scope, project_name)

    def reset_job(self, project_id):
        """Resets a job for a project. """
        [project_scope, project_name] = project_id.split('/')
        drive_api.reset_job(project_scope, project_name)


def main():
    """
    Command line interface for Factor Drive
    """
    config = get_config()
    drive_api.initialize_drive_api(
        drive_host=config.host,
        port=config.port,
        secure=config.secure,
        token=config.token
    )

    fire.Fire(Drive)


if __name__ == "__main__":
    main()
