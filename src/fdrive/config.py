# Copyright © 2024 Factor Technology, LLC
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""Read the .driverc.yaml file and return a Config object."""
from pathlib import Path
from dataclasses import dataclass, field
import sys
import yaml


@dataclass
class Config:
    """Configuration class"""
    token: str
    host: str = field(default='https://drive.factor.technology')
    port: int = field(default=443)
    secure: bool = field(default=True)
    
def _find_config_file(filename: str):
    # Start from the current directory
    current_dir = Path.cwd()

    # Check if the file exists in the current directory or any of its parents
    for parent in [current_dir] + list(current_dir.parents):
        potential_path = parent / filename
        if potential_path.exists():
            return potential_path

    # If the file was not found, check the user's home directory
    # (in case the cwd was not beneath it)
    home_dir = Path.home()
    potential_path = home_dir / filename
    if potential_path.exists():
        return potential_path

    # If the file was not found, raise an exception
    raise FileNotFoundError(f"Could not find {filename}")

def _read_config_file(path: Path):
    try:
        with open(path, 'r', encoding='utf-8') as file:
            config_dict = yaml.safe_load(file)
        return Config(**config_dict)
    except FileNotFoundError:
        print(f"Config file not found: {path}", file=sys.stderr)
        sys.exit(1)
    except yaml.YAMLError as e:
        print(f"Error parsing config file: {e}", file=sys.stderr)
        sys.exit(1)
    except TypeError as e:
        print(f"Error parsing config file: {e}", file=sys.stderr)
        sys.exit(1)
    except Exception as e:
        print(f"Error: {e}", file=sys.stderr)
        sys.exit(1)
        
def get_config():
	"""Return ther configuration as a dataclass"""
	config_file_path = _find_config_file('.driverc.yaml')
	return _read_config_file(config_file_path)
